# Use jbossdemocentral/developer as the base
FROM docker.io/jbossdemocentral/developer

# Maintainer details
MAINTAINER Andrew Block, Eric D. Schabell

# Environment Variables 
ENV RHDM_HOME /opt/jboss/rhdm/jboss-eap-7.3
ENV RHDM_VERSION_MAJOR 7
ENV RHDM_VERSION_MINOR 11
ENV RHDM_VERSION_MICRO 0

ENV EAP_VERSION_MAJOR 7
ENV EAP_VERSION_MINOR 3
ENV EAP_VERSION_MICRO 0

ENV EAP_ZIP=jboss-eap-$EAP_VERSION_MAJOR.$EAP_VERSION_MINOR.$EAP_VERSION_MICRO.zip
ENV RHDM_DEPLOYABLE=rhdm-$RHDM_VERSION_MAJOR.$RHDM_VERSION_MINOR.$RHDM_VERSION_MICRO-decision-central-eap7-deployable.zip
ENV RHDM_KIE_SERVER=rhdm-$RHDM_VERSION_MAJOR.$RHDM_VERSION_MINOR.$RHDM_VERSION_MICRO-kie-server-ee8.zip

# ADD Installation Files
COPY installs/$RHDM_DEPLOYABLE installs/$RHDM_KIE_SERVER installs/$EAP_ZIP /opt/jboss/

# Update Permissions on Installers
USER root
RUN chown 1000:1000 /opt/jboss/$EAP_ZIP /opt/jboss/$RHDM_DEPLOYABLE /opt/jboss/$RHDM_KIE_SERVER
USER 1000

# Prepare and run installer and cleanup installation components
RUN mkdir -p /opt/jboss/RHDM

RUN unzip -qo /opt/jboss/$EAP_ZIP -d /opt/jboss/rhdm \
    && unzip -qo /opt/jboss/$RHDM_DEPLOYABLE -d $RHDM_HOME/.. \
    && unzip -qo /opt/jboss/$RHDM_KIE_SERVER -d $RHDM_HOME/standalone/deployments \
    && touch $RHDM_HOME/standalone/deployments/kie-server.war.dodeploy \
    && rm -rf /opt/jboss/$RHDM_DEPLOYABLE /opt/jboss/$RHDM_KIE_SERVER /opt/jboss/$EAP_ZIP \
    && $RHDM_HOME/bin/add-user.sh -a -r ApplicationRealm -u erics -p redhatdm1! -ro analyst,admin,user,manager,kie-server,kiemgmt,rest-all --silent \
    && $RHDM_HOME/bin/add-user.sh -a -r ApplicationRealm -u kieserver -p kieserver1! -ro kie-server,rest-all --silent 

# Copy demo and support files
COPY support/standalone-full.xml $RHDM_HOME/standalone/configuration/standalone.xml
COPY support/userinfo.properties $RHDM_HOME/standalone/deployments/business-central.war/WEB-INF/classes/

# Swtich back to root user to perform build and cleanup
USER root

# Run Demo Maven build and cleanup
# 
# No project to build.

# Run as JBoss 
USER 1000

# Expose Ports
EXPOSE 9990 9999 8080 8001

# Run BPMS
CMD ["/opt/jboss/rhdm/jboss-eap-7.3/bin/standalone.sh","-c","standalone.xml","-b", "0.0.0.0","-bmanagement","0.0.0.0"]
