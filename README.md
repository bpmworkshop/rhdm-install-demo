Red Hat Decision Manager Install Demo 
=====================================
Project to automate the installation of this product with simple standalone configuration.


Install on your machine
-----------------------
1. [Download and unzip.](https://gitlab.com/bpmworkshop/rhdm-install-demo/-/archive/main/rhdm-install-demo-main.zip)

2. Add products to installs directory, see installs/README for details and links.

3. Run 'init.sh' or 'init.bat' file. 'init.bat' must be run with Administrative privileges

Login to http://localhost:8080/decision-central  (u:erics / p:redhatdm1!)

Kie Servier access configured (u: kieserver / p:kieserver1!)

Enjoy installed and configured Red Hat Decision Manager.


Install in a local container (Podman)
-------------------------------------
1. [Download and unzip.](https://gitlab.com/bpmworkshop/rhdm-install-demo/-/archive/main/rhdm-install-demo-main.zip)

2. Add products to installs directory, see installs/README for details and links.

3. [Install Podman on your machine](https://podman.io/getting-started/installation), the following instructions are OSX as an example.

4. Set up the virtual machine with 6GB memory:

  ```
  $ podman machine init --memory 6144 --disk-size 20
  ```

5. Ensure you have the proper networking setup for rootless containers:

  ```
   # In file ~/.config/containers/containers.conf make sure the CONTAINERS section 
   # has the line shown here:

   [containers]
   rootless_networking = "cni"
  ```

6. Start the virtual machine:

  ```
  $ podman machine start
  ```

7. Build the Red Hat Decision Manager container image from the root directory of the project:

  ```
  $ podman build -t rhdm-install:7.11 .
  ```

8. Verify image has been added to the local container regsitry:

  ```
  $ podman image list

   REPOSITORY                            TAG         IMAGE ID      CREATED        SIZE
   localhost/rhdm-install                7.11        74b4b8eda9a8  4 minutes ago  2.38 GB
   docker.io/jbossdemocentral/developer  latest      b73501ac39b1  5 years ago    514 MB
  ```

9. Run the rhdm-install image with ports mapped for access:

  ```
  $ podman run -dt -p 8080:8080 -p 9990:9990 rhdm-install
  ```

10. Verify logs of container that it's started before logging in (find CONTAINER_ID with 'podman container list':

  ```
  $ podman logs -f [CONTAINER_ID]
  ```

Login to http://localhost:8080/decision-central  (u:erics / p:redhatdm1!)

Enjoy installed and configured Red Hat Decision Manager.


Optional - Install Red Hat Decision Manager on OpenShift Container Platform
----------------------------------------------------------------------------
See the following project to install Red Hat Decision Manager in a container on OpenShift
Container Platform:

- [App Dev Cloud with Red Hat Decision Manager Install Demo](https://gitlab.com/redhatdemocentral/rhcs-rhdm-install-demo)


Supporting Articles
-------------------
- [Beginners Guide to Installing Decision Management Tooling in a Local Container using Podman](https://www.schabell.org/2021/10/beginners-guide-to-rhdm-local-conainter-podman.html.html)

- [Beginners guide - Building a retail web shop workshop update](https://www.schabell.org/2020/07/beginners-guide-building-a-retail-web-shop-workshop-update.html)

- [How to Install Red Hat Decision Manager 7.7](https://www.schabell.org/2020/07/how-to-install-rhdm-77.html)

- [How to Install Red Hat Decision Manager 7.6 in Minutes](http://bit.ly/how-to-install-red-hat-decision-manager-76-in-minutes)

- [How to Install Red Hat Decision Manager 7.5 in Minutes](http://bit.ly/how-to-install-red-hat-decision-manager-75-in-minutes)

- [AppDev in Cloud - How to put Red Hat Decision Manager in your Cloud](http://www.schabell.org/2018/11/appdev-in-cloud-how-to-put-red-hat-decision-manager-in-your-cloud.html)

- [Cloud Happiness - How to install OpenShift Container Platform with new images and templates in just minutes](http://www.schabell.org/2018/11/cloud-happiness-how-to-install-openshift-container-platform-with-new-images-templates-in-minutes.html)


Released versions
-----------------
See the tagged releases for the following versions of the product:

- v2.0 - JBoss EAP 7.3 and Red Hat Decision Manager 7.11 installed locally with podman container support.

- v1.9 - JBoss EAP 7.3 and Red Hat Decision Manager 7.11 installed locally.

- v1.8 - JBoss EAP 7.3 and Red Hat Decision Manager 7.10 installed locally.

- v1.7 - JBoss EAP 7.3 and Red Hat Decision Manager 7.9 installed locally.

- v1.6 - JBoss EAP 7.3 and Red Hat Decision Manager 7.8 installed locally.

- v1.5 - JBoss EAP 7.2 and Red Hat Decision Manager 7.7 installed locally.

- v1.4 - JBoss EAP 7.2 and Red Hat Decision Manager 7.6 installed locally.

- v1.3 - JBoss EAP 7.2 and Red Hat Decision Manager 7.5 installed locally.

- v1.2 - JBoss EAP 7.2 and Red Hat Decision Manager 7.3 installed locally.

- v1.1 - JBoss EAP 7.2 and Red Hat Decision Manager 7.2 installed locally.

- v1.0 - JBoss EAP 7.1.0 and Red Hat Decision Manager 7.1.0 installed locally.

![RHDM Login](https://gitlab.com/bpmworkshop/rhdm-install-demo/raw/master/docs/demo-images/rhdm-login.png)

![RHDM Decision Central](https://gitlab.com/bpmworkshop/rhdm-install-demo/raw/master/docs/demo-images/rhdm-decision-central.png)
